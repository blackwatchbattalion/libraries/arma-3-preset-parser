# ArmA 3 Preset Parser

This is a simple crate that was made to easily parse ArmA 3 presets into a format that was more easily ingestible by other programs.
As such, it exposes few methods which cover all the most-needed cases.

## Example
```rust
match arma_preset_parser::Preset::from_fs("some_path".parse().unwrap()) {
    Ok(preset) => println!("{:?}", preset),
    Err(e) => println!("{}", e)
};
```
